# deliveroo-rgr

This repository contains code as part of the Deliveroo take home technical test. The primary deliverable and final analyses are in the 'Deliveroo RGR' slide deck. These documents act as the technical support to those findings. 

The bulk of the analyses can be found in the deliveroo_rgr.ipynb notebook, but I have included some referral-specific EDA given that it was part of the exploratory process.

The data was saved in a GCP bucket on a personal instance, and this project was completed in AI Platform Notebooks. This was done primarily to take advantage of BigQuery so that I could check a few numbers and assumptions through the speed and ease that SQL provides.

## Notebooks

```
deliveroo_rgr.ipynb          # Contains general analyses, a propensity model, and a clustering model
rgr_EDA.ipynb                # Contains referral-specific EDA

```